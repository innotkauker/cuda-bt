#!/usr/bin/env bash

for field in tx1 tx2 tx3 ty1 ty2 ty3 tz1 tz2 tz3 dx1 dx2 dx3 dx4 dx5 dy1 dy2 dy3 dy4 dy5 dz1 dz2 dz3 dz4 dz5 dssp dt ce dxmax dymax dzmax xxcon dxtx yycon dyty zzcon dztz dnxm1 dnym1 dnzm1 c1c2 c1c5 c3c4 c1345 conz1 c1 c2 c3 c4 c5 c4dssp c5dssp dtdssp dttx1 bt dttx2 dtty1 dtty2 dttz1 dttz2 c2dttx1 c2dtty1 c2dttz1 comz1 comz4 comz5 comz6 c3c4tx3 c3c4ty3 c3c4tz3 c2iv con43 con16
do
    sed -i "s/\([^a-z0-9]\)$field\([^a-z0-9]\)/\1p.$field\2/g" x_solve.c y_solve.c z_solve.c adi.c verify.c error.c
done
