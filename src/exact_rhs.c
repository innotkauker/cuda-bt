#include "header.h"

//---------------------------------------------------------------------
// compute the right hand side based on exact solution
//---------------------------------------------------------------------
void exact_rhs()
{
    double dtemp[5], xi, eta, zeta, dtpp, ue_[5][5], buf_[5][5], cuf_[5], q_[5];
    int m, i, j, k, pp, ip1, im1, jp1, jm1, km1, kp1;

    for (k = 0; k <= nz - 1; k++)
    {
        for (j = 0; j <= ny - 1; j++)
        {
            for (i = 0; i <= nx - 1; i++)
            {
                for (m = 0; m < 5; m++)
                    forcing(k, j, i, m) = 0.0;
            }
        }
    }

    for (k = 1; k <= nz2; k++)
    {
        for (j = 1; j <= ny2; j++)
        {
            for (i = 1; i <= nx2; i++)
            {
                im1 = i - 1;
                ip1 = i + 1;

                for (pp = i - 2; pp <= i + 2; pp++)
                {
                    eta = j * p.dnym1;
                    zeta = k * p.dnzm1;
                    xi = pp * p.dnxm1;

                    exact_solution(xi, eta, zeta, dtemp);
                    dtpp = 1.0 / dtemp[0];
                    for (m = 0; m < 5; m++)
                    {
                        ue_[pp - i + 2][m] = dtemp[m];
                        if (m != 0)
                            buf_[pp - i + 2][m] = dtpp * dtemp[m];
                    }
                    cuf_[pp - i + 2] = buf_[pp - i + 2][1] * buf_[pp - i + 2][1];
                    buf_[pp - i + 2][0] = cuf_[pp - i + 2] + buf_[pp - i + 2][2] * buf_[pp - i + 2][2] + buf_[pp - i + 2][3] * buf_[pp - i + 2][3];
                    q_[pp - i + 2] = 0.5*(buf_[pp - i + 2][1] * ue_[pp - i + 2][1] + buf_[pp - i + 2][2] * ue_[pp - i + 2][2] + buf_[pp - i + 2][3] * ue_[pp - i + 2][3]);
                }

                forcing(k, j, i, 0) = forcing(k, j, i, 0) - p.tx2*(ue_[3][1] - ue_[1][1]) +
                    p.dxtx[0]*(ue_[3][0] - 2.0 * ue_[2][0] + ue_[1][0]);

                forcing(k, j, i, 1) = forcing(k, j, i, 1) - p.tx2 * (
                    (ue_[3][1] * buf_[3][1] + p.c2*(ue_[3][4] - q_[3])) -
                    (ue_[1][1] * buf_[1][1] + p.c2*(ue_[1][4] - q_[1]))) +
                    p.xxcon[0]*(buf_[3][1] - 2.0*buf_[2][1] + buf_[1][1]) +
                    p.dxtx[1]*(ue_[3][1] - 2.0* ue_[2][1] + ue_[1][1]);

                forcing(k, j, i, 2) = forcing(k, j, i, 2) - p.tx2 * (
                    ue_[3][2] * buf_[3][1] - ue_[1][2] * buf_[1][1]) +
                    p.xxcon[1]*(buf_[3][2] - 2.0*buf_[2][2] + buf_[1][2]) +
                    p.dxtx[2]*(ue_[3][2] - 2.0*ue_[2][2] + ue_[1][2]);

                forcing(k, j, i, 3) = forcing(k, j, i, 3) - p.tx2*(
                    ue_[3][3] * buf_[3][1] - ue_[1][3] * buf_[1][1]) +
                    p.xxcon[1]*(buf_[3][3] - 2.0*buf_[2][3] + buf_[1][3]) +
                    p.dxtx[3]*(ue_[3][3] - 2.0* ue_[2][3] + ue_[1][3]);

                forcing(k, j, i, 4) = forcing(k, j, i, 4) - p.tx2*(
                    buf_[3][1] * (p.c1*ue_[3][4] - p.c2*q_[3]) -
                    buf_[1][1] * (p.c1*ue_[1][4] - p.c2*q_[1])) +
                    0.5*p.xxcon[2]*(buf_[3][0] - 2.0*buf_[2][0] + buf_[1][0]) +
                    p.xxcon[3]*(cuf_[3] - 2.0*cuf_[2] + cuf_[1]) +
                    p.xxcon[4]*(buf_[3][4] - 2.0*buf_[2][4] + buf_[1][4]) +
                    p.dxtx[4]*(ue_[3][4] - 2.0* ue_[2][4] + ue_[1][4]);

                if (i == 1)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (5.0*ue_[2][m] - 4.0*ue_[3][m] + ue_[4][m]);
                }
                else if (i == 2)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (-4.0*ue_[1][m] + 6.0*ue_[2][m] - 4.0*ue_[3][m] + ue_[4][m]);
                }
                else if (i == nx - 3)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (ue_[0][m] - 4.0*ue_[1][m] + 6.0*ue_[2][m] - 4.0*ue_[3][m]);
                }
                else if (i == nx - 2)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (ue_[0][m] - 4.0*ue_[1][m] + 5.0*ue_[2][m]);
                }
                else
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (ue_[0][m] - 4.0*ue_[1][m] + 6.0*ue_[2][m] - 4.0*ue_[3][m] + ue_[4][m]);
                }


                jm1 = j - 1;
                jp1 = j + 1;

                for (pp = j - 2; pp <= j + 2; pp++)
                {
                    eta = pp * p.dnym1;
                    zeta = k * p.dnzm1;
                    xi = i * p.dnxm1;

                    exact_solution(xi, eta, zeta, dtemp);
                    dtpp = 1.0 / dtemp[0];
                    for (m = 0; m < 5; m++)
                    {
                        ue_[pp - j + 2][m] = dtemp[m];
                        if (m != 0)
                            buf_[pp - j + 2][m] = dtpp * dtemp[m];
                    }
                    cuf_[pp - j + 2] = buf_[pp - j + 2][2] * buf_[pp - j + 2][2];
                    buf_[pp - j + 2][0] = cuf_[pp - j + 2] + buf_[pp - j + 2][1] * buf_[pp - j + 2][1] + buf_[pp - j + 2][3] * buf_[pp - j + 2][3];
                    q_[pp - j + 2] = 0.5*(buf_[pp - j + 2][1] * ue_[pp - j + 2][1] + buf_[pp - j + 2][2] * ue_[pp - j + 2][2] + buf_[pp - j + 2][3] * ue_[pp - j + 2][3]);
                }

                forcing(k, j, i, 0) = forcing(k, j, i, 0) -
                    p.ty2*(ue_[3][2] - ue_[1][2]) +
                    p.dyty[0]*(ue_[3][0] - 2.0*ue_[2][0] + ue_[1][0]);

                forcing(k, j, i, 1) = forcing(k, j, i, 1) - p.ty2*(
                    ue_[3][1] * buf_[3][2] - ue_[1][1] * buf_[1][2]) +
                    p.yycon[1]*(buf_[3][1] - 2.0*buf_[2][1] + buf_[1][1]) +
                    p.dyty[1]*(ue_[3][1] - 2.0* ue_[2][1] + ue_[1][1]);

                forcing(k, j, i, 2) = forcing(k, j, i, 2) - p.ty2*(
                    (ue_[3][2] * buf_[3][2] + p.c2*(ue_[3][4] - q_[3])) -
                    (ue_[1][2] * buf_[1][2] + p.c2*(ue_[1][4] - q_[1]))) +
                    p.yycon[0]*(buf_[3][2] - 2.0*buf_[2][2] + buf_[1][2]) +
                    p.dyty[2]*(ue_[3][2] - 2.0*ue_[2][2] + ue_[1][2]);

                forcing(k, j, i, 3) = forcing(k, j, i, 3) - p.ty2*(
                    ue_[3][3] * buf_[3][2] - ue_[1][3] * buf_[1][2]) +
                    p.yycon[1]*(buf_[3][3] - 2.0*buf_[2][3] + buf_[1][3]) +
                    p.dyty[3]*(ue_[3][3] - 2.0*ue_[2][3] + ue_[1][3]);

                forcing(k, j, i, 4) = forcing(k, j, i, 4) - p.ty2*(
                    buf_[3][2] * (p.c1*ue_[3][4] - p.c2*q_[3]) -
                    buf_[1][2] * (p.c1*ue_[1][4] - p.c2*q_[1])) +
                    0.5*p.yycon[2]*(buf_[3][0] - 2.0*buf_[2][0] +
                    buf_[1][0]) +
                    p.yycon[3]*(cuf_[3] - 2.0*cuf_[2] + cuf_[1]) +
                    p.yycon[4]*(buf_[3][4] - 2.0*buf_[2][4] + buf_[1][4]) +
                    p.dyty[4]*(ue_[3][4] - 2.0*ue_[2][4] + ue_[1][4]);

                if (j == 1)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (5.0*ue_[2][m] - 4.0*ue_[3][m] + ue_[4][m]);
                }
                else if (j == 2)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (-4.0*ue_[1][m] + 6.0*ue_[2][m] - 4.0*ue_[3][m] + ue_[4][m]);
                }
                else if (j == ny - 3)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (ue_[0][m] - 4.0*ue_[1][m] + 6.0*ue_[2][m] - 4.0*ue_[3][m]);
                }
                else if (j == ny - 2)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (ue_[0][m] - 4.0*ue_[1][m] + 5.0*ue_[2][m]);
                }
                else
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (ue_[0][m] - 4.0*ue_[1][m] + 6.0*ue_[2][m] - 4.0*ue_[3][m] + ue_[4][m]);
                }

                km1 = k - 1;
                kp1 = k + 1;

                for (pp = k - 2; pp <= k + 2; pp++)
                {
                    eta = j * p.dnym1;
                    zeta = pp * p.dnzm1;
                    xi = i * p.dnxm1;

                    exact_solution(xi, eta, zeta, dtemp);
                    dtpp = 1.0 / dtemp[0];
                    for (m = 0; m < 5; m++)
                    {
                        ue_[pp - k + 2][m] = dtemp[m];
                        if (m != 0)
                            buf_[pp - k + 2][m] = dtpp * dtemp[m];
                    }
                    cuf_[pp - k + 2] = buf_[pp - k + 2][3] * buf_[pp - k + 2][3];
                    buf_[pp - k + 2][0] = cuf_[pp - k + 2] + buf_[pp - k + 2][1] * buf_[pp - k + 2][1] + buf_[pp - k + 2][2] * buf_[pp - k + 2][2];
                    q_[pp - k + 2] = 0.5*(buf_[pp - k + 2][1] * ue_[pp - k + 2][1] + buf_[pp - k + 2][2] * ue_[pp - k + 2][2] + buf_[pp - k + 2][3] * ue_[pp - k + 2][3]);
                }

                forcing(k, j, i, 0) = forcing(k, j, i, 0) -
                    p.tz2*(ue_[3][3] - ue_[1][3]) +
                    p.dztz[0]*(ue_[3][0] - 2.0*ue_[2][0] + ue_[1][0]);

                forcing(k, j, i, 1) = forcing(k, j, i, 1) - p.tz2 * (
                    ue_[3][1] * buf_[3][3] - ue_[1][1] * buf_[1][3]) +
                    p.zzcon[1]*(buf_[3][1] - 2.0*buf_[2][1] + buf_[1][1]) +
                    p.dztz[1]*(ue_[3][1] - 2.0* ue_[2][1] + ue_[1][1]);

                forcing(k, j, i, 2) = forcing(k, j, i, 2) - p.tz2 * (
                    ue_[3][2] * buf_[3][3] - ue_[1][2] * buf_[1][3]) +
                    p.zzcon[1]*(buf_[3][2] - 2.0*buf_[2][2] + buf_[1][2]) +
                    p.dztz[2]*(ue_[3][2] - 2.0*ue_[2][2] + ue_[1][2]);

                forcing(k, j, i, 3) = forcing(k, j, i, 3) - p.tz2 * (
                    (ue_[3][3] * buf_[3][3] + p.c2*(ue_[3][4] - q_[3])) -
                    (ue_[1][3] * buf_[1][3] + p.c2*(ue_[1][4] - q_[1]))) +
                    p.zzcon[0]*(buf_[3][3] - 2.0*buf_[2][3] + buf_[1][3]) +
                    p.dztz[3]*(ue_[3][3] - 2.0*ue_[2][3] + ue_[1][3]);

                forcing(k, j, i, 4) = forcing(k, j, i, 4) - p.tz2 * (
                    buf_[3][3] * (p.c1*ue_[3][4] - p.c2*q_[3]) -
                    buf_[1][3] * (p.c1*ue_[1][4] - p.c2*q_[1])) +
                    0.5*p.zzcon[2]*(buf_[3][0] - 2.0*buf_[2][0] + buf_[1][0]) +
                    p.zzcon[3]*(cuf_[3] - 2.0*cuf_[2] + cuf_[1]) +
                    p.zzcon[4]*(buf_[3][4] - 2.0*buf_[2][4] + buf_[1][4]) +
                    p.dztz[4]*(ue_[3][4] - 2.0*ue_[2][4] + ue_[1][4]);

                if (k == 1)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (5.0*ue_[2][m] - 4.0*ue_[3][m] + ue_[4][m]);
                }
                else if (k == 2)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (-4.0*ue_[1][m] + 6.0*ue_[2][m] - 4.0*ue_[3][m] + ue_[4][m]);
                }
                else if (k == nz - 3)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (ue_[0][m] - 4.0*ue_[1][m] + 6.0*ue_[2][m] - 4.0*ue_[3][m]);
                }
                else if (k == nz - 2)
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (ue_[0][m] - 4.0*ue_[1][m] + 5.0*ue_[2][m]);
                }
                else
                {
                    for (m = 0; m < 5; m++)
                        forcing(k, j, i, m) = forcing(k, j, i, m) - p.dssp * (ue_[0][m] - 4.0*ue_[1][m] + 6.0*ue_[2][m] - 4.0*ue_[3][m] + ue_[4][m]);
                }

                for (m = 0; m < 5; m++)
                    forcing(k, j, i, m) = -1.0 * forcing(k, j, i, m);
            }
        }
    }
}
