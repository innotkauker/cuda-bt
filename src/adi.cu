#include <thrust/device_vector.h>
#include "header.h"

//---------------------------------------------------------------------
// block-diagonal matrix-vector multiplication                  
//---------------------------------------------------------------------
__global__ void xinvr(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
                      double (*us)      [P_SIZE][P_SIZE],
                      double (*vs)      [P_SIZE][P_SIZE],
                      double (*ws)      [P_SIZE][P_SIZE],
                      double (*qs)      [P_SIZE][P_SIZE],
                      double (*rho_i)   [P_SIZE][P_SIZE],
                      double (*speed)   [P_SIZE][P_SIZE],
                      double (*square)  [P_SIZE][P_SIZE],
                      double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                      double (*forcing) [P_SIZE][P_SIZE][P_SIZE],
                      struct params *pp,
                      unsigned nx2,
                      unsigned ny2,
                      unsigned nz2)
{
    unsigned k = blockIdx.x * blockDim.x + threadIdx.x + 1;
    unsigned j = blockIdx.y * blockDim.y + threadIdx.y + 1;
    if (k > nz2 || j > ny2)
        return;

    int i;
    double t1, t2, t3, ac, ru1, uu, vv, ww, r1, r2, r3, r4, r5, ac2inv;

    for (i = 1; i <= nx2; i++)
    {
        ru1 = rho_i[k][j][i];
        uu = us[k][j][i];
        vv = vs[k][j][i];
        ww = ws[k][j][i];
        ac = speed[k][j][i];
        ac2inv = ac*ac;

        r1 = rhs(k, j, i, 0);
        r2 = rhs(k, j, i, 1);
        r3 = rhs(k, j, i, 2);
        r4 = rhs(k, j, i, 3);
        r5 = rhs(k, j, i, 4);

        t1 = pp->c2 / ac2inv * (qs[k][j][i] * r1 - uu*r2 - vv*r3 - ww*r4 + r5);
        t2 = pp->bt * ru1 * (uu * r1 - r2);
        t3 = (pp->bt * ru1 * ac) * t1;

        rhs(k, j, i, 0) = r1 - t1;
        rhs(k, j, i, 1) = -ru1 * (ww*r1 - r4);
        rhs(k, j, i, 2) = ru1 * (vv*r1 - r3);
        rhs(k, j, i, 3) = -t2 + t3;
        rhs(k, j, i, 4) = t2 + t3;
    }
}

__global__ void add(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
                    double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                    unsigned nx2,
                    unsigned ny2,
                    unsigned nz2)
{
    unsigned k = blockIdx.x * blockDim.x + threadIdx.x + 1;
    unsigned j = blockIdx.y * blockDim.y + threadIdx.y + 1;
    if (k > nz2 || j > ny2)
        return;

    int i, m;

    for (i = 1; i <= nx2; i++) {
        for (m = 0; m < 5; m++) {
            u(k, j, i, m) = u(k, j, i, m) + rhs(k, j, i, m);
        }
    }

}

void adi(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
         double (*us)      [P_SIZE][P_SIZE],
         double (*vs)      [P_SIZE][P_SIZE],
         double (*ws)      [P_SIZE][P_SIZE],
         double (*qs)      [P_SIZE][P_SIZE],
         double (*rho_i)   [P_SIZE][P_SIZE],
         double (*speed)   [P_SIZE][P_SIZE],
         double (*square)  [P_SIZE][P_SIZE],
         double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
         double (*forcing) [P_SIZE][P_SIZE][P_SIZE],
         params *pp,
         unsigned nx2,
         unsigned ny2,
         unsigned nz2)
{
    compute_rhs(u, us, vs, ws, qs, rho_i, speed, square, rhs, forcing, pp, nx2, ny2, nz2);

    dim3 threads_per_block;
    dim3 blocks;
    compute_grid(threads_per_block, blocks, nz2, ny2);

    if (timeron) timer_start(t_txinvr);
    xinvr<<<blocks, threads_per_block>>>(u, us, vs, ws, qs, rho_i, speed, square, rhs, forcing, pp, nx2, ny2, nz2);
    if (timeron) timer_stop(t_txinvr);

    x_solve(u, us, vs, ws, qs, rho_i, speed, square, rhs, forcing, pp, nx2, ny2, nz2);
    y_solve(u, us, vs, ws, qs, rho_i, speed, square, rhs, forcing, pp, nx2, ny2, nz2);
    z_solve(u, us, vs, ws, qs, rho_i, speed, square, rhs, forcing, pp, nx2, ny2, nz2);

    if (timeron) timer_start(t_add);
    add<<<blocks, threads_per_block>>>(u, rhs, nx2, ny2, nz2);
    if (timeron) timer_stop(t_add);
}
