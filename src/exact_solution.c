#include "header.h"

//---------------------------------------------------------------------
// this function returns the exact solution at point xi, eta, zeta
//---------------------------------------------------------------------
void exact_solution(double xi, double eta, double zeta, double dtemp[5])
{
    int m;

    for (m = 0; m < 5; m++)
    {
        dtemp[m] = p.ce[m][0] +
            xi  *(p.ce[m][1] + xi  *(p.ce[m][4] + xi  *(p.ce[m][7] + xi  *p.ce[m][10]))) +
            eta *(p.ce[m][2] + eta *(p.ce[m][5] + eta *(p.ce[m][8] + eta *p.ce[m][11]))) +
            zeta*(p.ce[m][3] + zeta*(p.ce[m][6] + zeta*(p.ce[m][9] + zeta*p.ce[m][12])));
    }
}
