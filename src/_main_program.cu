//---------------------------------------------------------------------
// MAIN program
//---------------------------------------------------------------------

#include <thrust/device_vector.h>
#include <cstdio>
#include <cstdlib>
#include "header.h"

/* global parameters */
int nx2, ny2, nz2, nx, ny, nz;
logical timeron;

/* constants */
struct params p;

/* main arrays */
double (*u)       [P_SIZE][P_SIZE][P_SIZE];
double (*us)      [P_SIZE][P_SIZE];
double (*vs)      [P_SIZE][P_SIZE];
double (*ws)      [P_SIZE][P_SIZE];
double (*qs)      [P_SIZE][P_SIZE];
double (*rho_i)   [P_SIZE][P_SIZE];
double (*speed)   [P_SIZE][P_SIZE];
double (*square)  [P_SIZE][P_SIZE];
double (*rhs)     [P_SIZE][P_SIZE][P_SIZE];
double (*forcing) [P_SIZE][P_SIZE][P_SIZE];

/* tmp arrays lhs */
double lhs_ [P_SIZE][5];
double lhsp_[P_SIZE][5];
double lhsm_[P_SIZE][5];

int main(int argc, char *argv[])
{
    printf("\n Program started \n");

    int i, niter, step, result;
    double tmax;
    logical verified;
    const char *t_names[t_last + 1];

    timeron = inittrace(t_names);
    result = initparameters(argc, argv, &niter);
    if (result == 0)
        return -1;
    result = allocateArrays();
    if (result == 0)
        return -2;

    for (i = 1; i <= t_last; i++)
        timer_clear(i);

    // init
    set_constants();
    exact_rhs();
    initialize();

    // copy the data to device
    thrust::device_vector<double> u_dev((double*)u, (double*)(u) + nx * ny * nz * 5);
    double (*u_ptr)[P_SIZE][P_SIZE][P_SIZE] = (double (*)[P_SIZE][P_SIZE][P_SIZE])thrust::raw_pointer_cast(u_dev.data());
    thrust::device_vector<double> rhs_dev((double*)rhs, (double*)(rhs) + nx * ny * nz * 5);
    double (*rhs_ptr)[P_SIZE][P_SIZE][P_SIZE] = (double (*)[P_SIZE][P_SIZE][P_SIZE])thrust::raw_pointer_cast(rhs_dev.data());
    thrust::device_vector<double> forcing_dev((double*)forcing, (double*)(forcing) + nx * ny * nz * 5);
    double (*forcing_ptr)[P_SIZE][P_SIZE][P_SIZE] = (double (*)[P_SIZE][P_SIZE][P_SIZE])thrust::raw_pointer_cast(forcing_dev.data());
    thrust::device_vector<double> us_dev((double*)us, (double*)(us) + nx * ny * nz);
    double (*us_ptr)[P_SIZE][P_SIZE] = (double (*)[P_SIZE][P_SIZE])thrust::raw_pointer_cast(us_dev.data());
    thrust::device_vector<double> vs_dev((double*)vs, (double*)(vs) + nx * ny * nz);
    double (*vs_ptr)[P_SIZE][P_SIZE] = (double (*)[P_SIZE][P_SIZE])thrust::raw_pointer_cast(vs_dev.data());
    thrust::device_vector<double> ws_dev((double*)ws, (double*)(ws) + nx * ny * nz);
    double (*ws_ptr)[P_SIZE][P_SIZE] = (double (*)[P_SIZE][P_SIZE])thrust::raw_pointer_cast(ws_dev.data());
    thrust::device_vector<double> qs_dev((double*)qs, (double*)(qs) + nx * ny * nz);
    double (*qs_ptr)[P_SIZE][P_SIZE] = (double (*)[P_SIZE][P_SIZE])thrust::raw_pointer_cast(qs_dev.data());
    thrust::device_vector<double> rho_i_dev((double*)rho_i, (double*)(rho_i) + nx * ny * nz);
    double (*rho_i_ptr)[P_SIZE][P_SIZE] = (double (*)[P_SIZE][P_SIZE])thrust::raw_pointer_cast(rho_i_dev.data());
    thrust::device_vector<double> speed_dev((double*)speed, (double*)(speed) + nx * ny * nz);
    double (*speed_ptr)[P_SIZE][P_SIZE] = (double (*)[P_SIZE][P_SIZE])thrust::raw_pointer_cast(speed_dev.data());
    thrust::device_vector<double> square_dev((double*)square, (double*)(square) + nx * ny * nz);
    double (*square_ptr)[P_SIZE][P_SIZE] = (double (*)[P_SIZE][P_SIZE])thrust::raw_pointer_cast(square_dev.data());
    struct params *pp{nullptr};
    cudaMalloc((void**)(&pp), sizeof(struct params));
    cudaMemcpy(pp, &p, sizeof(struct params), cudaMemcpyHostToDevice);

    // main loop
    timer_start(t_total);
    for (step = 1; step <= niter; step++)
    {
        if ( (step % 20) == 0 || step == 1)
            printf(" Time step %4d\n", step);
        adi(u_ptr, us_ptr, vs_ptr, ws_ptr, qs_ptr, rho_i_ptr, speed_ptr, square_ptr, rhs_ptr, forcing_ptr, pp, nx2, ny2, nz2);
    }
    timer_stop(t_total);
    tmax = timer_read(t_total);

    // rescue data from the device
    thrust::copy(u_dev.begin(), u_dev.end(), (double *)u);
    thrust::copy(us_dev.begin(), us_dev.end(), (double *)us);
    thrust::copy(vs_dev.begin(), vs_dev.end(), (double *)vs);
    thrust::copy(ws_dev.begin(), ws_dev.end(), (double *)ws);
    thrust::copy(qs_dev.begin(), qs_dev.end(), (double *)qs);
    thrust::copy(rho_i_dev.begin(), rho_i_dev.end(), (double *)rho_i);
    thrust::copy(speed_dev.begin(), speed_dev.end(), (double *)speed);
    thrust::copy(square_dev.begin(), square_dev.end(), (double *)square);
    thrust::copy(rhs_dev.begin(), rhs_dev.end(), (double *)rhs);
    thrust::copy(forcing_dev.begin(), forcing_dev.end(), (double *)forcing);
    cudaFree(pp);

    verify(niter, &verified);
    print_results(niter, tmax, verified, t_names);

    result = deallocateArrays();
     if (result == 0)
        return -2;
    return 0;
}
