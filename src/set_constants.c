#include "header.h"
#include <math.h>

void set_constants()
{
    p.ce[0][0] = 2.0;
    p.ce[0][1] = 0.0;
    p.ce[0][2] = 0.0;
    p.ce[0][3] = 4.0;
    p.ce[0][4] = 5.0;
    p.ce[0][5] = 3.0;
    p.ce[0][6] = 0.5;
    p.ce[0][7] = 0.02;
    p.ce[0][8] = 0.01;
    p.ce[0][9] = 0.03;
    p.ce[0][10] = 0.5;
    p.ce[0][11] = 0.4;
    p.ce[0][12] = 0.3;

    p.ce[1][0] = 1.0;
    p.ce[1][1] = 0.0;
    p.ce[1][2] = 0.0;
    p.ce[1][3] = 0.0;
    p.ce[1][4] = 1.0;
    p.ce[1][5] = 2.0;
    p.ce[1][6] = 3.0;
    p.ce[1][7] = 0.01;
    p.ce[1][8] = 0.03;
    p.ce[1][9] = 0.02;
    p.ce[1][10] = 0.4;
    p.ce[1][11] = 0.3;
    p.ce[1][12] = 0.5;

    p.ce[2][0] = 2.0;
    p.ce[2][1] = 2.0;
    p.ce[2][2] = 0.0;
    p.ce[2][3] = 0.0;
    p.ce[2][4] = 0.0;
    p.ce[2][5] = 2.0;
    p.ce[2][6] = 3.0;
    p.ce[2][7] = 0.04;
    p.ce[2][8] = 0.03;
    p.ce[2][9] = 0.05;
    p.ce[2][10] = 0.3;
    p.ce[2][11] = 0.5;
    p.ce[2][12] = 0.4;

    p.ce[3][0] = 2.0;
    p.ce[3][1] = 2.0;
    p.ce[3][2] = 0.0;
    p.ce[3][3] = 0.0;
    p.ce[3][4] = 0.0;
    p.ce[3][5] = 2.0;
    p.ce[3][6] = 3.0;
    p.ce[3][7] = 0.03;
    p.ce[3][8] = 0.05;
    p.ce[3][9] = 0.04;
    p.ce[3][10] = 0.2;
    p.ce[3][11] = 0.1;
    p.ce[3][12] = 0.3;

    p.ce[4][0] = 5.0;
    p.ce[4][1] = 4.0;
    p.ce[4][2] = 3.0;
    p.ce[4][3] = 2.0;
    p.ce[4][4] = 0.1;
    p.ce[4][5] = 0.4;
    p.ce[4][6] = 0.3;
    p.ce[4][7] = 0.05;
    p.ce[4][8] = 0.04;
    p.ce[4][9] = 0.03;
    p.ce[4][10] = 0.1;
    p.ce[4][11] = 0.3;
    p.ce[4][12] = 0.2;

    p.c1 = 1.4;
    p.c2 = 0.4;
    p.c3 = 0.1;
    p.c4 = 1.0;
    p.c5 = 1.4;

    p.bt = sqrt(0.5);

    p.dnxm1 = 1.0 / (double)(nx - 1);
    p.dnym1 = 1.0 / (double)(ny - 1);
    p.dnzm1 = 1.0 / (double)(nz - 1);

    p.c1c2 = p.c1 * p.c2;
    p.c1c5 = p.c1 * p.c5;
    p.c3c4 = p.c3 * p.c4;
    p.c1345 = p.c1c5 * p.c3c4;

    p.conz1 = (1.0 - p.c1c5);

    p.tx1 = 1.0 / (p.dnxm1 * p.dnxm1);
    p.tx2 = 1.0 / (2.0 * p.dnxm1);
    p.tx3 = 1.0 / p.dnxm1;

    p.ty1 = 1.0 / (p.dnym1 * p.dnym1);
    p.ty2 = 1.0 / (2.0 * p.dnym1);
    p.ty3 = 1.0 / p.dnym1;

    p.tz1 = 1.0 / (p.dnzm1 * p.dnzm1);
    p.tz2 = 1.0 / (2.0 * p.dnzm1);
    p.tz3 = 1.0 / p.dnzm1;

    p.dx1 = 0.75;
    p.dx2 = 0.75;
    p.dx3 = 0.75;
    p.dx4 = 0.75;
    p.dx5 = 0.75;

    p.dy1 = 0.75;
    p.dy2 = 0.75;
    p.dy3 = 0.75;
    p.dy4 = 0.75;
    p.dy5 = 0.75;

    p.dz1 = 1.0;
    p.dz2 = 1.0;
    p.dz3 = 1.0;
    p.dz4 = 1.0;
    p.dz5 = 1.0;

    p.dxmax = max(p.dx3, p.dx4);
    p.dymax = max(p.dy2, p.dy4);
    p.dzmax = max(p.dz2, p.dz3);

    p.dssp = 0.25 * max(p.dx1, max(p.dy1, p.dz1));

    p.c4dssp = 4.0 * p.dssp;
    p.c5dssp = 5.0 * p.dssp;

    p.dttx1 = p.dt*p.tx1;
    p.dttx2 = p.dt*p.tx2;
    p.dtty1 = p.dt*p.ty1;
    p.dtty2 = p.dt*p.ty2;
    p.dttz1 = p.dt*p.tz1;
    p.dttz2 = p.dt*p.tz2;

    p.c2dttx1 = 2.0*p.dttx1;
    p.c2dtty1 = 2.0*p.dtty1;
    p.c2dttz1 = 2.0*p.dttz1;

    p.dtdssp = p.dt*p.dssp;

    p.comz1 = p.dtdssp;
    p.comz4 = 4.0*p.dtdssp;
    p.comz5 = 5.0*p.dtdssp;
    p.comz6 = 6.0*p.dtdssp;

    p.c3c4tx3 = p.c3c4*p.tx3;
    p.c3c4ty3 = p.c3c4*p.ty3;
    p.c3c4tz3 = p.c3c4*p.tz3;

    p.dxtx[0] = p.dx1*p.tx1;
    p.dxtx[1] = p.dx2*p.tx1;
    p.dxtx[2] = p.dx3*p.tx1;
    p.dxtx[3] = p.dx4*p.tx1;
    p.dxtx[4] = p.dx5*p.tx1;

    p.dyty[0] = p.dy1*p.ty1;
    p.dyty[1] = p.dy2*p.ty1;
    p.dyty[2] = p.dy3*p.ty1;
    p.dyty[3] = p.dy4*p.ty1;
    p.dyty[4] = p.dy5*p.ty1;

    p.dztz[0] = p.dz1*p.tz1;
    p.dztz[1] = p.dz2*p.tz1;
    p.dztz[2] = p.dz3*p.tz1;
    p.dztz[3] = p.dz4*p.tz1;
    p.dztz[4] = p.dz5*p.tz1;

    p.c2iv = 2.5;
    p.con43 = 4.0 / 3.0;
    p.con16 = 1.0 / 6.0;

    p.xxcon[0] = p.c3c4tx3*p.con43*p.tx3;
    p.xxcon[1] = p.c3c4tx3*p.tx3;
    p.xxcon[2] = p.c3c4tx3*p.conz1*p.tx3;
    p.xxcon[3] = p.c3c4tx3*p.con16*p.tx3;
    p.xxcon[4] = p.c3c4tx3*p.c1c5*p.tx3;

    p.yycon[0] = p.c3c4ty3*p.con43*p.ty3;
    p.yycon[1] = p.c3c4ty3*p.ty3;
    p.yycon[2] = p.c3c4ty3*p.conz1*p.ty3;
    p.yycon[3] = p.c3c4ty3*p.con16*p.ty3;
    p.yycon[4] = p.c3c4ty3*p.c1c5*p.ty3;

    p.zzcon[0] = p.c3c4tz3*p.con43*p.tz3;
    p.zzcon[1] = p.c3c4tz3*p.tz3;
    p.zzcon[2] = p.c3c4tz3*p.conz1*p.tz3;
    p.zzcon[3] = p.c3c4tz3*p.con16*p.tz3;
    p.zzcon[4] = p.c3c4tz3*p.c1c5*p.tz3;
}
