#include <thrust/device_vector.h>
#include <cmath>
#include "header.h"

__global__ void x_main(double (*us)      [P_SIZE][P_SIZE],
                       double (*vs)      [P_SIZE][P_SIZE],
                       double (*ws)      [P_SIZE][P_SIZE],
                       double (*qs)      [P_SIZE][P_SIZE],
                       double (*rho_i)   [P_SIZE][P_SIZE],
                       double (*speed)   [P_SIZE][P_SIZE],
                       double (*square)  [P_SIZE][P_SIZE],
                       double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                       params *pp,
                       unsigned nx2,
                       unsigned ny2,
                       unsigned nz2)
{
    unsigned j = blockIdx.x * blockDim.x + threadIdx.x + 1;
    unsigned k = blockIdx.y * blockDim.y + threadIdx.y + 1;
    if (k > nz2 || j > ny2)
        return;

    const unsigned nx = nx2 + 2;
    double lhs_ [P_SIZE][5];
    double lhsp_[P_SIZE][5];
    double lhsm_[P_SIZE][5];
    for (unsigned m = 0; m < 5; m++)
    {
        lhs_ [0][m] = lhs_ [nx2 + 1][m] = 0.0;
        lhsp_[0][m] = lhsp_[nx2 + 1][m] = 0.0;
        lhsm_[0][m] = lhsm_[nx2 + 1][m] = 0.0;
    }

    lhs_ [0][2] = lhs_ [nx2 + 1][2] = 1.0;
    lhsp_[0][2] = lhsp_[nx2 + 1][2] = 1.0;
    lhsm_[0][2] = lhsm_[nx2 + 1][2] = 1.0;
        
    for (unsigned i = 1; i <= nx2; i++)
    {
        lhs_[i][0] = 0.0;
        double ru1 = pp->c3c4*rho_i[k][j][i - 1];
        double rhon1 = max(max(pp->dx2 + pp->con43 * ru1, pp->dx5 + pp->c1c5 * ru1), max(pp->dxmax + ru1, pp->dx1));
        lhs_[i][1] = -pp->dttx2 * us[k][j][i - 1] - pp->dttx1 * rhon1;

        ru1 = pp->c3c4*rho_i[k][j][i];
        rhon1 = max(max(pp->dx2 + pp->con43 * ru1, pp->dx5 + pp->c1c5 * ru1), max(pp->dxmax + ru1, pp->dx1));
        lhs_[i][2] = 1.0 + pp->c2dttx1 * rhon1;

        ru1 = pp->c3c4*rho_i[k][j][i + 1];
        rhon1 = max(max(pp->dx2 + pp->con43 * ru1, pp->dx5 + pp->c1c5 * ru1), max(pp->dxmax + ru1, pp->dx1));
        lhs_[i][3] = pp->dttx2 * us[k][j][i + 1] - pp->dttx1 * rhon1;
        lhs_[i][4] = 0.0;

        if (i == 1)
        {
            lhs_[i][2] = lhs_[i][2] + pp->comz5;
            lhs_[i][3] = lhs_[i][3] - pp->comz4;
            lhs_[i][4] = lhs_[i][4] + pp->comz1;
        }
        else if (i == 2)
        {
            lhs_[i][1] = lhs_[i][1] - pp->comz4;
            lhs_[i][2] = lhs_[i][2] + pp->comz6;
            lhs_[i][3] = lhs_[i][3] - pp->comz4;
            lhs_[i][4] = lhs_[i][4] + pp->comz1;
        }
        else if (i == nx - 3)
        {
            lhs_[i][0] = lhs_[i][0] + pp->comz1;
            lhs_[i][1] = lhs_[i][1] - pp->comz4;
            lhs_[i][2] = lhs_[i][2] + pp->comz6;
            lhs_[i][3] = lhs_[i][3] - pp->comz4;
        }
        else if (i == nx - 2)
        {
            lhs_[i][0] = lhs_[i][0] + pp->comz1;
            lhs_[i][1] = lhs_[i][1] - pp->comz4;
            lhs_[i][2] = lhs_[i][2] + pp->comz5;
        }
        else
        {
            lhs_[i][0] = lhs_[i][0] + pp->comz1;
            lhs_[i][1] = lhs_[i][1] - pp->comz4;
            lhs_[i][2] = lhs_[i][2] + pp->comz6;
            lhs_[i][3] = lhs_[i][3] - pp->comz4;
            lhs_[i][4] = lhs_[i][4] + pp->comz1;

        }

        lhsp_[i][0] = lhs_[i][0];
        lhsp_[i][1] = lhs_[i][1] - pp->dttx2 * speed[k][j][i - 1];
        lhsp_[i][2] = lhs_[i][2];
        lhsp_[i][3] = lhs_[i][3] + pp->dttx2 * speed[k][j][i + 1];
        lhsp_[i][4] = lhs_[i][4];

        lhsm_[i][0] = lhs_[i][0];
        lhsm_[i][1] = lhs_[i][1] + pp->dttx2 * speed[k][j][i - 1];
        lhsm_[i][2] = lhs_[i][2];
        lhsm_[i][3] = lhs_[i][3] - pp->dttx2 * speed[k][j][i + 1];
        lhsm_[i][4] = lhs_[i][4];
    }

    for (unsigned i = 1; i <= nx2; i++)
    {
        unsigned i1 = i;
        unsigned i2 = i + 1;
        double fac1 = 1.0 / lhs_[i - 1][2];
        lhs_[i - 1][3] = fac1 * lhs_[i - 1][3];
        lhs_[i - 1][4] = fac1 * lhs_[i - 1][4];
        for (unsigned m = 0; m < 3; m++)
            rhs(k, j, i - 1, m) = fac1*rhs(k, j, i - 1, m);

        lhs_[i1][2] = lhs_[i1][2] - lhs_[i1][1] * lhs_[i - 1][3];
        lhs_[i1][3] = lhs_[i1][3] - lhs_[i1][1] * lhs_[i - 1][4];
        for (unsigned m = 0; m < 3; m++)
            rhs(k, j, i1, m) = rhs(k, j, i1, m) - lhs_[i1][1] * rhs(k, j, i - 1, m);

        lhs_[i2][1] = lhs_[i2][1] - lhs_[i2][0] * lhs_[i - 1][3];
        lhs_[i2][2] = lhs_[i2][2] - lhs_[i2][0] * lhs_[i - 1][4];
        for (unsigned m = 0; m < 3; m++)
            rhs(k, j, i2, m) = rhs(k, j, i2, m) - lhs_[i2][0] * rhs(k, j, i - 1, m);

        if (i == nx2)
        {
            fac1 = 1.0 / lhs_[i1][2];
            lhs_[i1][3] = fac1 * lhs_[i1][3];
            lhs_[i1][4] = fac1 * lhs_[i1][4];
            for (unsigned m = 0; m < 3; m++)
                rhs(k, j, i1, m) = fac1 * rhs(k, j, i1, m);

            lhs_[i2][2] = lhs_[i2][2] - lhs_[i2][1] * lhs_[i1][3];
            lhs_[i2][3] = lhs_[i2][3] - lhs_[i2][1] * lhs_[i1][4];
            for (unsigned m = 0; m < 3; m++)
                rhs(k, j, i2, m) = rhs(k, j, i2, m) - lhs_[i2][1] * rhs(k, j, i1, m);

            double fac2 = 1.0 / lhs_[i2][2];
            for (unsigned m = 0; m < 3; m++)
                rhs(k, j, i2, m) = fac2*rhs(k, j, i2, m);
        }
    
        unsigned m = 3;
        fac1 = 1.0 / lhsp_[i - 1][2];
        lhsp_[i - 1][3] = fac1 * lhsp_[i - 1][3];
        lhsp_[i - 1][4] = fac1 * lhsp_[i - 1][4];
        rhs(k, j, i - 1, m) = fac1 * rhs(k, j, i - 1, m);

        lhsp_[i1][2] = lhsp_[i1][2] - lhsp_[i1][1] * lhsp_[i - 1][3];
        lhsp_[i1][3] = lhsp_[i1][3] - lhsp_[i1][1] * lhsp_[i - 1][4];
        rhs(k, j, i1, m) = rhs(k, j, i1, m) - lhsp_[i1][1] * rhs(k, j, i - 1, m);

        lhsp_[i2][1] = lhsp_[i2][1] - lhsp_[i2][0] * lhsp_[i - 1][3];
        lhsp_[i2][2] = lhsp_[i2][2] - lhsp_[i2][0] * lhsp_[i - 1][4];
        rhs(k, j, i2, m) = rhs(k, j, i2, m) - lhsp_[i2][0] * rhs(k, j, i - 1, m);

        m = 4;
        fac1 = 1.0 / lhsm_[i - 1][2];
        lhsm_[i - 1][3] = fac1*lhsm_[i - 1][3];
        lhsm_[i - 1][4] = fac1*lhsm_[i - 1][4];
        rhs(k, j, i - 1, m) = fac1*rhs(k, j, i - 1, m);
        lhsm_[i1][2] = lhsm_[i1][2] - lhsm_[i1][1] * lhsm_[i - 1][3];
        lhsm_[i1][3] = lhsm_[i1][3] - lhsm_[i1][1] * lhsm_[i - 1][4];
        rhs(k, j, i1, m) = rhs(k, j, i1, m) - lhsm_[i1][1] * rhs(k, j, i - 1, m);
        lhsm_[i2][1] = lhsm_[i2][1] - lhsm_[i2][0] * lhsm_[i - 1][3];
        lhsm_[i2][2] = lhsm_[i2][2] - lhsm_[i2][0] * lhsm_[i - 1][4];
        rhs(k, j, i2, m) = rhs(k, j, i2, m) - lhsm_[i2][0] * rhs(k, j, i - 1, m);

        if (i == nx2)
        {
            m = 3;
            fac1 = 1.0 / lhsp_[i1][2];
            lhsp_[i1][3] = fac1 * lhsp_[i1][3];
            lhsp_[i1][4] = fac1 * lhsp_[i1][4];
            rhs(k, j, i1, m) = fac1 * rhs(k, j, i1, m);

            lhsp_[i2][2] = lhsp_[i2][2] - lhsp_[i2][1] * lhsp_[i1][3];
            lhsp_[i2][3] = lhsp_[i2][3] - lhsp_[i2][1] * lhsp_[i1][4];
            rhs(k, j, i2, m) = rhs(k, j, i2, m) - lhsp_[i2][1] * rhs(k, j, i1, m);

            m = 4;
            fac1 = 1.0 / lhsm_[i1][2];
            lhsm_[i1][3] = fac1 * lhsm_[i1][3];
            lhsm_[i1][4] = fac1 * lhsm_[i1][4];
            rhs(k, j, i1, m) = fac1*rhs(k, j, i1, m);

            lhsm_[i2][2] = lhsm_[i2][2] - lhsm_[i2][1] * lhsm_[i1][3];
            lhsm_[i2][3] = lhsm_[i2][3] - lhsm_[i2][1] * lhsm_[i1][4];
            rhs(k, j, i2, m) = rhs(k, j, i2, m) - lhsm_[i2][1] * rhs(k, j, i1, m);

            rhs(k, j, i2, 3) = rhs(k, j, i2, 3) / lhsp_[i2][2];
            rhs(k, j, i2, 4) = rhs(k, j, i2, 4) / lhsm_[i2][2];

            for (unsigned m = 0; m < 3; m++)
                rhs(k, j, i1, m) = rhs(k, j, i1, m) - lhs_[i1][3] * rhs(k, j, i2, m);

            rhs(k, j, i1, 3) = rhs(k, j, i1, 3) - lhsp_[i1][3] * rhs(k, j, i2, 3);
            rhs(k, j, i1, 4) = rhs(k, j, i1, 4) - lhsm_[i1][3] * rhs(k, j, i2, 4);
        }
    }

    for (unsigned i = nx2; i >= 1; i--)
    {
        unsigned i1 = i;
        unsigned i2 = i + 1;
        for (unsigned m = 0; m < 3; m++)
            rhs(k, j, i - 1, m) = rhs(k, j, i - 1, m) - lhs_[i - 1][3] * rhs(k, j, i1, m) - lhs_[i - 1][4] * rhs(k, j, i2, m);

        rhs(k, j, i - 1, 3) = rhs(k, j, i - 1, 3) - lhsp_[i - 1][3] * rhs(k, j, i1, 3) - lhsp_[i - 1][4] * rhs(k, j, i2, 3);
        rhs(k, j, i - 1, 4) = rhs(k, j, i - 1, 4) - lhsm_[i - 1][3] * rhs(k, j, i1, 4) - lhsm_[i - 1][4] * rhs(k, j, i2, 4);
    }
}

__global__ void x_post(double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                       double bt,
                       unsigned nx2,
                       unsigned ny2,
                       unsigned nz2)
{
    unsigned i = blockIdx.x * blockDim.x + threadIdx.x + 1;
    unsigned j = blockIdx.y * blockDim.y + threadIdx.y + 1;
    unsigned k = blockIdx.z * blockDim.z + threadIdx.z + 1;
    if (k > nz2 || j > ny2 || i > nx2)
        return;

    double r1 = rhs(k, j, i, 0);
    double r2 = rhs(k, j, i, 1);
    double r3 = rhs(k, j, i, 2);
    double r4 = rhs(k, j, i, 3);
    double r5 = rhs(k, j, i, 4);

    double t1 = bt * r3;
    double t2 = 0.5 * (r4 + r5);

    rhs(k, j, i, 0) = -r2;
    rhs(k, j, i, 1) = r1;
    rhs(k, j, i, 2) = bt * (r4 - r5);
    rhs(k, j, i, 3) = -t1 + t2;
    rhs(k, j, i, 4) = t1 + t2;
}

//---------------------------------------------------------------------
// this function performs the solution of the approximate factorization
// step in the x-direction for all five matrix components
// simultaneously. The Thomas algorithm is employed to solve the
// systems for the x-lines. Boundary conditions are non-periodic
//---------------------------------------------------------------------
void x_solve(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
             double (*us)      [P_SIZE][P_SIZE],
             double (*vs)      [P_SIZE][P_SIZE],
             double (*ws)      [P_SIZE][P_SIZE],
             double (*qs)      [P_SIZE][P_SIZE],
             double (*rho_i)   [P_SIZE][P_SIZE],
             double (*speed)   [P_SIZE][P_SIZE],
             double (*square)  [P_SIZE][P_SIZE],
             double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
             double (*forcing) [P_SIZE][P_SIZE][P_SIZE],
             params *pp,
             unsigned nx2,
             unsigned ny2,
             unsigned nz2)
{
    if (timeron) timer_start(t_xsolve);
    dim3 threads_per_block;
    dim3 blocks;
    compute_grid(threads_per_block, blocks, ny2, nz2);

    x_main<<<blocks, threads_per_block>>>(us, vs, ws, qs, rho_i, speed, square, rhs, pp, nx2, ny2, nz2);

    //---------------------------------------------------------------------
    // Do the block-diagonal inversion          
    //---------------------------------------------------------------------
    if (timeron) timer_start(t_ninvr);

    compute_grid(threads_per_block, blocks, nx2, ny2, nz2);
    x_post<<<blocks, threads_per_block>>>(rhs, p.bt, nx2, ny2, nz2);
    if (timeron) timer_stop(t_ninvr);
    if (timeron) timer_stop(t_xsolve);
}
