#include <math.h>
#include <thrust/device_vector.h>
#include "header.h"

__global__ void init_rhs(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
                         double (*us)      [P_SIZE][P_SIZE],
                         double (*vs)      [P_SIZE][P_SIZE],
                         double (*ws)      [P_SIZE][P_SIZE],
                         double (*qs)      [P_SIZE][P_SIZE],
                         double (*rho_i)   [P_SIZE][P_SIZE],
                         double (*speed)   [P_SIZE][P_SIZE],
                         double (*square)  [P_SIZE][P_SIZE],
                         double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                         double (*forcing) [P_SIZE][P_SIZE][P_SIZE],
                         double c1c2,
                         unsigned nx,
                         unsigned ny,
                         unsigned nz)
{
    unsigned i = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned j = blockIdx.y * blockDim.y + threadIdx.y;
    unsigned k = blockIdx.z * blockDim.z + threadIdx.z;
    if (k >= nz || j >= ny || i >= nx)
        return;
    double rho_inv = 1.0 / u(k, j, i, 0);
    rho_i[k][j][i] = rho_inv;
    us[k][j][i] = u(k, j, i, 1) * rho_inv;
    vs[k][j][i] = u(k, j, i, 2) * rho_inv;
    ws[k][j][i] = u(k, j, i, 3) * rho_inv;
    square[k][j][i] = 0.5* (u(k, j, i, 1) * u(k, j, i, 1) + u(k, j, i, 2) * u(k, j, i, 2) + u(k, j, i, 3) * u(k, j, i, 3)) * rho_inv;
    qs[k][j][i] = square[k][j][i] * rho_inv;
    double aux = c1c2*rho_inv* (u(k, j, i, 4) - square[k][j][i]);
    speed[k][j][i] = sqrt(aux);

    for (unsigned m = 0; m < 5; m++)
        rhs(k, j, i, m) = forcing(k, j, i, m);
}

__global__ void process_i(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
                          double (*us)      [P_SIZE][P_SIZE],
                          double (*vs)      [P_SIZE][P_SIZE],
                          double (*ws)      [P_SIZE][P_SIZE],
                          double (*qs)      [P_SIZE][P_SIZE],
                          double (*rho_i)   [P_SIZE][P_SIZE],
                          double (*speed)   [P_SIZE][P_SIZE],
                          double (*square)  [P_SIZE][P_SIZE],
                          double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                          double (*forcing) [P_SIZE][P_SIZE][P_SIZE],
                          params *pp,
                          unsigned nx2,
                          unsigned ny2,
                          unsigned nz2)
{
    unsigned j = blockIdx.x * blockDim.x + threadIdx.x + 1;
    unsigned k = blockIdx.y * blockDim.y + threadIdx.y + 1;
    if (k > ny2 || j > nx2)
        return;
    for (unsigned i = 1; i <= nx2; i++)
    {
        double uijk = us[k][j][i];
        double up1 = us[k][j][i + 1];
        double um1 = us[k][j][i - 1];

        rhs(k, j, i, 0) = rhs(k, j, i, 0) + pp->dxtx[0] *
            (u(k, j, i + 1, 0) - 2.0*u(k, j, i, 0) + u(k, j, i - 1, 0)) -
            pp->tx2 * (u(k, j, i + 1, 1) - u(k, j, i - 1, 1));

        rhs(k, j, i, 1) = rhs(k, j, i, 1) + pp->dxtx[1] *
            (u(k, j, i + 1, 1) - 2.0*u(k, j, i, 1) + u(k, j, i - 1, 1)) +
            pp->xxcon[1]*pp->con43 * (up1 - 2.0*uijk + um1) -
            pp->tx2 * (u(k, j, i + 1, 1) * up1 - u(k, j, i - 1, 1) * um1 +
            (u(k, j, i + 1, 4) - square[k][j][i + 1] -
            u(k, j, i - 1, 4) + square[k][j][i - 1]) * pp->c2);

        rhs(k, j, i, 2) = rhs(k, j, i, 2) + pp->dxtx[2] *
            (u(k, j, i + 1, 2) - 2.0*u(k, j, i, 2) + u(k, j, i - 1, 2)) +
            pp->xxcon[1] * (vs[k][j][i + 1] - 2.0*vs[k][j][i] + vs[k][j][i - 1]) -
            pp->tx2 * (u(k, j, i + 1, 2) * up1 - u(k, j, i - 1, 2) * um1);

        rhs(k, j, i, 3) = rhs(k, j, i, 3) + pp->dxtx[3] *
            (u(k, j, i + 1, 3) - 2.0*u(k, j, i, 3) + u(k, j, i - 1, 3)) +
            pp->xxcon[1] * (ws[k][j][i + 1] - 2.0*ws[k][j][i] + ws[k][j][i - 1]) -
            pp->tx2 * (u(k, j, i + 1, 3) * up1 - u(k, j, i - 1, 3) * um1);

        rhs(k, j, i, 4) = rhs(k, j, i, 4) + pp->dxtx[4] *
            (u(k, j, i + 1, 4) - 2.0*u(k, j, i, 4) + u(k, j, i - 1, 4)) +
            pp->xxcon[2] * (qs[k][j][i + 1] - 2.0*qs[k][j][i] + qs[k][j][i - 1]) +
            pp->xxcon[3] * (up1*up1 - 2.0*uijk*uijk + um1*um1) +
            pp->xxcon[4] * (u(k, j, i + 1, 4) * rho_i[k][j][i + 1] -
            2.0*u(k, j, i, 4) * rho_i[k][j][i] +
            u(k, j, i - 1, 4) * rho_i[k][j][i - 1]) -
            pp->tx2 * ((pp->c1*u(k, j, i + 1, 4) - pp->c2*square[k][j][i + 1])*up1 -
            (pp->c1*u(k, j, i - 1, 4) - pp->c2*square[k][j][i - 1])*um1);

        unsigned m;
        if (i == 1)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (5.0*u(k, j, i, m) - 4.0*u(k, j, i + 1, m) + u(k, j, i + 2, m));
        }
        else if (i == 2)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (-4.0*u(k, j, i - 1, m) + 6.0*u(k, j, i, m) - 4.0*u(k, j, i + 1, m) + u(k, j, i + 2, m));
        }
        else if (i == nx2 - 1)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (u(k, j, i - 2, m) - 4.0*u(k, j, i - 1, m) + 6.0*u(k, j, i, m) - 4.0*u(k, j, i + 1, m));
        }
        else if (i == nx2)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (u(k, j, i - 2, m) - 4.0*u(k, j, i - 1, m) + 5.0*u(k, j, i, m));
        }
        else
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (u(k, j, i - 2, m) - 4.0*u(k, j, i - 1, m) + 6.0*u(k, j, i, m) - 4.0*u(k, j, i + 1, m) + u(k, j, i + 2, m));
        }
    }
}

__global__ void process_j(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
                          double (*us)      [P_SIZE][P_SIZE],
                          double (*vs)      [P_SIZE][P_SIZE],
                          double (*ws)      [P_SIZE][P_SIZE],
                          double (*qs)      [P_SIZE][P_SIZE],
                          double (*rho_i)   [P_SIZE][P_SIZE],
                          double (*speed)   [P_SIZE][P_SIZE],
                          double (*square)  [P_SIZE][P_SIZE],
                          double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                          double (*forcing) [P_SIZE][P_SIZE][P_SIZE],
                          params *pp,
                          unsigned nx2,
                          unsigned ny2,
                          unsigned nz2)
{
    unsigned i = blockIdx.x * blockDim.x + threadIdx.x + 1;
    unsigned k = blockIdx.y * blockDim.y + threadIdx.y + 1;
    if (k > ny2 || i > nx2)
        return;
    for (unsigned j = 1; j <= ny2; j++)
    {
        double vijk = vs[k][j][i];
        double vp1 = vs[k][j + 1][i];
        double vm1 = vs[k][j - 1][i];

        rhs(k, j, i, 0) = rhs(k, j, i, 0) + pp->dyty[0] *
            (u(k, j + 1, i, 0) - 2.0*u(k, j, i, 0) + u(k, j - 1, i, 0)) -
            pp->ty2 * (u(k, j + 1, i, 2) - u(k, j - 1, i, 2));

        rhs(k, j, i, 1) = rhs(k, j, i, 1) + pp->dyty[1] *
            (u(k, j + 1, i, 1) - 2.0*u(k, j, i, 1) + u(k, j - 1, i, 1)) +
            pp->yycon[1] * (us[k][j + 1][i] - 2.0*us[k][j][i] + us[k][j - 1][i]) -
            pp->ty2 * (u(k, j + 1, i, 1) * vp1 - u(k, j - 1, i, 1) * vm1);

        rhs(k, j, i, 2) = rhs(k, j, i, 2) + pp->dyty[2] *
            (u(k, j + 1, i, 2) - 2.0*u(k, j, i, 2) + u(k, j - 1, i, 2)) +
            pp->yycon[1]*pp->con43 * (vp1 - 2.0*vijk + vm1) -
            pp->ty2 * (u(k, j + 1, i, 2) * vp1 - u(k, j - 1, i, 2) * vm1 +
            (u(k, j + 1, i, 4) - square[k][j + 1][i] -
            u(k, j - 1, i, 4) + square[k][j - 1][i]) * pp->c2);

        rhs(k, j, i, 3) = rhs(k, j, i, 3) + pp->dyty[3] *
            (u(k, j + 1, i, 3) - 2.0*u(k, j, i, 3) + u(k, j - 1, i, 3)) +
            pp->yycon[1] * (ws[k][j + 1][i] - 2.0*ws[k][j][i] + ws[k][j - 1][i]) -
            pp->ty2 * (u(k, j + 1, i, 3) * vp1 - u(k, j - 1, i, 3) * vm1);

        rhs(k, j, i, 4) = rhs(k, j, i, 4) + pp->dyty[4] *
            (u(k, j + 1, i, 4) - 2.0*u(k, j, i, 4) + u(k, j - 1, i, 4)) +
            pp->yycon[2] * (qs[k][j + 1][i] - 2.0*qs[k][j][i] + qs[k][j - 1][i]) +
            pp->yycon[3] * (vp1*vp1 - 2.0*vijk*vijk + vm1*vm1) +
            pp->yycon[4] * (u(k, j + 1, i, 4) * rho_i[k][j + 1][i] -
            2.0*u(k, j, i, 4) * rho_i[k][j][i] +
            u(k, j - 1, i, 4) * rho_i[k][j - 1][i]) -
            pp->ty2 * ((pp->c1*u(k, j + 1, i, 4) - pp->c2*square[k][j + 1][i]) * vp1 -
            (pp->c1*u(k, j - 1, i, 4) - pp->c2*square[k][j - 1][i]) * vm1);

        unsigned m;
        if (j == 1)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (5.0*u(k, j, i, m) - 4.0*u(k, j + 1, i, m) + u(k, j + 2, i, m));
        }
        else if (j == 2)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (-4.0*u(k, j - 1, i, m) + 6.0*u(k, j, i, m) - 4.0*u(k, j + 1, i, m) + u(k, j + 2, i, m));
        }
        else if (j == ny2 - 1)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (u(k, j - 2, i, m) - 4.0*u(k, j - 1, i, m) + 6.0*u(k, j, i, m) - 4.0*u(k, j + 1, i, m));
        }
        else if (j == ny2)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (u(k, j - 2, i, m) - 4.0*u(k, j - 1, i, m) + 5.0*u(k, j, i, m));
        }
        else
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (u(k, j - 2, i, m) - 4.0*u(k, j - 1, i, m) + 6.0*u(k, j, i, m) - 4.0*u(k, j + 1, i, m) + u(k, j + 2, i, m));
        }
    }
}

__global__ void process_k(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
                          double (*us)      [P_SIZE][P_SIZE],
                          double (*vs)      [P_SIZE][P_SIZE],
                          double (*ws)      [P_SIZE][P_SIZE],
                          double (*qs)      [P_SIZE][P_SIZE],
                          double (*rho_i)   [P_SIZE][P_SIZE],
                          double (*speed)   [P_SIZE][P_SIZE],
                          double (*square)  [P_SIZE][P_SIZE],
                          double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                          double (*forcing) [P_SIZE][P_SIZE][P_SIZE],
                          params *pp,
                          unsigned nx2,
                          unsigned ny2,
                          unsigned nz2)
{
    unsigned i = blockIdx.x * blockDim.x + threadIdx.x + 1;
    unsigned j = blockIdx.y * blockDim.y + threadIdx.y + 1;
    if (j > ny2 || i > nx2)
        return;
    for (unsigned k = 1; k <= nz2; k++)
    {
        double wijk = ws[k][j][i];
        double wp1 = ws[k + 1][j][i];
        double wm1 = ws[k - 1][j][i];

        rhs(k, j, i, 0) = rhs(k, j, i, 0) + pp->dztz[0] *
            (u(k + 1, j, i, 0) - 2.0*u(k, j, i, 0) + u(k - 1, j, i, 0)) -
            pp->tz2 * (u(k + 1, j, i, 3) - u(k - 1, j, i, 3));

        rhs(k, j, i, 1) = rhs(k, j, i, 1) + pp->dztz[1] *
            (u(k + 1, j, i, 1) - 2.0*u(k, j, i, 1) + u(k - 1, j, i, 1)) +
            pp->zzcon[1] * (us[k + 1][j][i] - 2.0*us[k][j][i] + us[k - 1][j][i]) -
            pp->tz2 * (u(k + 1, j, i, 1) * wp1 - u(k - 1, j, i, 1) * wm1);

        rhs(k, j, i, 2) = rhs(k, j, i, 2) + pp->dztz[2] *
            (u(k + 1, j, i, 2) - 2.0*u(k, j, i, 2) + u(k - 1, j, i, 2)) +
            pp->zzcon[1] * (vs[k + 1][j][i] - 2.0*vs[k][j][i] + vs[k - 1][j][i]) -
            pp->tz2 * (u(k + 1, j, i, 2) * wp1 - u(k - 1, j, i, 2) * wm1);

        rhs(k, j, i, 3) = rhs(k, j, i, 3) + pp->dztz[3] *
            (u(k + 1, j, i, 3) - 2.0*u(k, j, i, 3) + u(k - 1, j, i, 3)) +
            pp->zzcon[1]*pp->con43 * (wp1 - 2.0*wijk + wm1) -
            pp->tz2 * (u(k + 1, j, i, 3) * wp1 - u(k - 1, j, i, 3) * wm1 +
            (u(k + 1, j, i, 4) - square[k + 1][j][i] -
            u(k - 1, j, i, 4) + square[k - 1][j][i]) * pp->c2);

        rhs(k, j, i, 4) = rhs(k, j, i, 4) + pp->dztz[4] *
            (u(k + 1, j, i, 4) - 2.0*u(k, j, i, 4) + u(k - 1, j, i, 4)) +
            pp->zzcon[2] * (qs[k + 1][j][i] - 2.0*qs[k][j][i] + qs[k - 1][j][i]) +
            pp->zzcon[3] * (wp1*wp1 - 2.0*wijk*wijk + wm1*wm1) +
            pp->zzcon[4] * (u(k + 1, j, i, 4) * rho_i[k + 1][j][i] -
            2.0*u(k, j, i, 4) * rho_i[k][j][i] +
            u(k - 1, j, i, 4) * rho_i[k - 1][j][i]) -
            pp->tz2 * ((pp->c1*u(k + 1, j, i, 4) - pp->c2*square[k + 1][j][i])*wp1 -
            (pp->c1*u(k - 1, j, i, 4) - pp->c2*square[k - 1][j][i])*wm1);

        unsigned m;
        if (k == 1)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (5.0*u(k, j, i, m) - 4.0*u(k + 1, j, i, m) + u(k + 2, j, i, m));
        }
        else if (k == 2)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (-4.0*u(k - 1, j, i, m) + 6.0*u(k, j, i, m) - 4.0*u(k + 1, j, i, m) + u(k + 2, j, i, m));
        }
        else if (k == nz2 - 1)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (u(k - 2, j, i, m) - 4.0*u(k - 1, j, i, m) + 6.0*u(k, j, i, m) - 4.0*u(k + 1, j, i, m));
        }
        else if (k == nz2)
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (u(k - 2, j, i, m) - 4.0*u(k - 1, j, i, m) + 5.0*u(k, j, i, m));
        }
        else
        {
            for (m = 0; m < 5; m++)
                rhs(k, j, i, m) = rhs(k, j, i, m) - pp->dssp * (u(k - 2, j, i, m) - 4.0*u(k - 1, j, i, m) + 6.0*u(k, j, i, m) - 4.0*u(k + 1, j, i, m) + u(k + 2, j, i, m));
        }
    }
}

__global__ void finish_rhs(double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                           double dt,
                           unsigned nx2,
                           unsigned ny2,
                           unsigned nz2)
{
    unsigned i = blockIdx.x * blockDim.x + threadIdx.x + 1;
    unsigned j = blockIdx.y * blockDim.y + threadIdx.y + 1;
    unsigned k = blockIdx.z * blockDim.z + threadIdx.z + 1;
    if (k > nz2 || j > ny2 || i > nx2)
        return;
    for (unsigned m = 0; m < 5; m++)
    {
        rhs(k, j, i, m) = rhs(k, j, i, m) * dt;
    }
}

void compute_rhs(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
                 double (*us)      [P_SIZE][P_SIZE],
                 double (*vs)      [P_SIZE][P_SIZE],
                 double (*ws)      [P_SIZE][P_SIZE],
                 double (*qs)      [P_SIZE][P_SIZE],
                 double (*rho_i)   [P_SIZE][P_SIZE],
                 double (*speed)   [P_SIZE][P_SIZE],
                 double (*square)  [P_SIZE][P_SIZE],
                 double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                 double (*forcing) [P_SIZE][P_SIZE][P_SIZE],
                 params *pp,
                 unsigned nx2,
                 unsigned ny2,
                 unsigned nz2)
{
    if (timeron) timer_start(t_rhs);
    dim3 threads_per_block;
    dim3 blocks;
    compute_grid(threads_per_block, blocks, nx, ny, nz);

    init_rhs<<<blocks, threads_per_block>>>(u, us, vs, ws, qs, rho_i, speed, square, rhs, forcing, p.c1c2, nx, ny, nz);

    if (timeron) timer_start(t_rhsx);
    compute_grid(threads_per_block, blocks, ny2, nz2);
    process_i<<<blocks, threads_per_block>>>(u, us, vs, ws, qs, rho_i, speed, square, rhs, forcing, pp, nx2, ny2, nz2);
    if (timeron) timer_stop(t_rhsx);

    if (timeron) timer_start(t_rhsy);
    compute_grid(threads_per_block, blocks, nx2, nz2);
    process_j<<<blocks, threads_per_block>>>(u, us, vs, ws, qs, rho_i, speed, square, rhs, forcing, pp, nx2, ny2, nz2);
    if (timeron) timer_stop(t_rhsy);

    if (timeron) timer_start(t_rhsz);
    compute_grid(threads_per_block, blocks, nx2, ny2);
    process_k<<<blocks, threads_per_block>>>(u, us, vs, ws, qs, rho_i, speed, square, rhs, forcing, pp, nx2, ny2, nz2);
    if (timeron) timer_stop(t_rhsz);

    compute_grid(threads_per_block, blocks, nx2, ny2, nz2);
    finish_rhs<<<blocks, threads_per_block>>>(rhs, p.dt, nx2, ny2, nz2);
    if (timeron) timer_stop(t_rhs);
}
