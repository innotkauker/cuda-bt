#include <thrust/device_vector.h>
#include "header.h"

__global__ void z_main(double (*us)      [P_SIZE][P_SIZE],
                       double (*vs)      [P_SIZE][P_SIZE],
                       double (*ws)      [P_SIZE][P_SIZE],
                       double (*qs)      [P_SIZE][P_SIZE],
                       double (*rho_i)   [P_SIZE][P_SIZE],
                       double (*speed)   [P_SIZE][P_SIZE],
                       double (*square)  [P_SIZE][P_SIZE],
                       double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                       params *pp,
                       unsigned nx2,
                       unsigned ny2,
                       unsigned nz2)
{
    unsigned i = blockIdx.x * blockDim.x + threadIdx.x + 1;
    unsigned j = blockIdx.y * blockDim.y + threadIdx.y + 1;
    if (j > ny2 || i > nx2)
        return;

    int k, k1, k2, m;
    double ru1, rhos1, fac1, fac2;

    //const unsigned nz = nz2 + 2;
    double lhs_ [P_SIZE][5];
    double lhsp_[P_SIZE][5];
    double lhsm_[P_SIZE][5];
    for (m = 0; m < 5; m++)
    {
        lhs_[0][m] = lhs_[nz2 + 1][m] = 0.0;
        lhsp_[0][m] = lhsp_[nz2 + 1][m] = 0.0;
        lhsm_[0][m] = lhsm_[nz2 + 1][m] = 0.0;
    }
    lhs_[0][2] = lhs_[nz2 + 1][2] = 1.0;
    lhsp_[0][2] = lhsp_[nz2 + 1][2] = 1.0;
    lhsm_[0][2] = lhsm_[nz2 + 1][2] = 1.0;

    for (k = 1; k <= nz2; k++)
    {
        lhs_[k][0] = 0.0;

        ru1 = pp->c3c4*rho_i[k - 1][j][i];
        rhos1 = max(max(pp->dz4 + pp->con43*ru1, pp->dz5 + pp->c1c5*ru1), max(pp->dzmax + ru1, pp->dz1));
        lhs_[k][1] = -pp->dttz2 * ws[k - 1][j][i] - pp->dttz1 * rhos1;

        ru1 = pp->c3c4*rho_i[k][j][i];
        rhos1 = max(max(pp->dz4 + pp->con43*ru1, pp->dz5 + pp->c1c5*ru1), max(pp->dzmax + ru1, pp->dz1));
        lhs_[k][2] = 1.0 + pp->c2dttz1 * rhos1;

        ru1 = pp->c3c4*rho_i[k + 1][j][i];
        rhos1 = max(max(pp->dz4 + pp->con43*ru1, pp->dz5 + pp->c1c5*ru1), max(pp->dzmax + ru1, pp->dz1));
        lhs_[k][3] = pp->dttz2 * ws[k + 1][j][i] - pp->dttz1 * rhos1;
        lhs_[k][4] = 0.0;

        if (k == 1)
        {
            lhs_[k][2] = lhs_[k][2] + pp->comz5;
            lhs_[k][3] = lhs_[k][3] - pp->comz4;
            lhs_[k][4] = lhs_[k][4] + pp->comz1;
        }
        else if (k == 2)
        {
            lhs_[k][1] = lhs_[k][1] - pp->comz4;
            lhs_[k][2] = lhs_[k][2] + pp->comz6;
            lhs_[k][3] = lhs_[k][3] - pp->comz4;
            lhs_[k][4] = lhs_[k][4] + pp->comz1;
        }
        else if (k == nz2 - 1)
        {
            lhs_[k][0] = lhs_[k][0] + pp->comz1;
            lhs_[k][1] = lhs_[k][1] - pp->comz4;
            lhs_[k][2] = lhs_[k][2] + pp->comz6;
            lhs_[k][3] = lhs_[k][3] - pp->comz4;
        }
        else if (k == nz2)
        {
            lhs_[k][0] = lhs_[k][0] + pp->comz1;
            lhs_[k][1] = lhs_[k][1] - pp->comz4;
            lhs_[k][2] = lhs_[k][2] + pp->comz5;
        }
        else
        {
            lhs_[k][0] = lhs_[k][0] + pp->comz1;
            lhs_[k][1] = lhs_[k][1] - pp->comz4;
            lhs_[k][2] = lhs_[k][2] + pp->comz6;
            lhs_[k][3] = lhs_[k][3] - pp->comz4;
            lhs_[k][4] = lhs_[k][4] + pp->comz1;
        }

        lhsp_[k][0] = lhs_[k][0];
        lhsp_[k][1] = lhs_[k][1] - pp->dttz2 * speed[k - 1][j][i];
        lhsp_[k][2] = lhs_[k][2];
        lhsp_[k][3] = lhs_[k][3] + pp->dttz2 * speed[k + 1][j][i];
        lhsp_[k][4] = lhs_[k][4];
        lhsm_[k][0] = lhs_[k][0];
        lhsm_[k][1] = lhs_[k][1] + pp->dttz2 * speed[k - 1][j][i];
        lhsm_[k][2] = lhs_[k][2];
        lhsm_[k][3] = lhs_[k][3] - pp->dttz2 * speed[k + 1][j][i];
        lhsm_[k][4] = lhs_[k][4];
    }

    for (k = 1; k <= nz2; k++)
    {
        k1 = k;
        k2 = k + 1;

        fac1 = 1.0 / lhs_[k - 1][2];
        lhs_[k - 1][3] = fac1 * lhs_[k - 1][3];
        lhs_[k - 1][4] = fac1 * lhs_[k - 1][4];
        for (m = 0; m < 3; m++)
            rhs(k - 1, j, i, m) = fac1 * rhs(k - 1, j, i, m);

        lhs_[k1][2] = lhs_[k1][2] - lhs_[k1][1] * lhs_[k - 1][3];
        lhs_[k1][3] = lhs_[k1][3] - lhs_[k1][1] * lhs_[k - 1][4];
        for (m = 0; m < 3; m++)
            rhs(k1, j, i, m) = rhs(k1, j, i, m) - lhs_[k1][1] * rhs(k - 1, j, i, m);

        lhs_[k2][1] = lhs_[k2][1] - lhs_[k2][0] * lhs_[k - 1][3];
        lhs_[k2][2] = lhs_[k2][2] - lhs_[k2][0] * lhs_[k - 1][4];
        for (m = 0; m < 3; m++)
            rhs(k2, j, i, m) = rhs(k2, j, i, m) - lhs_[k2][0] * rhs(k - 1, j, i, m);

        if (k == nz2)
        {
            fac1 = 1.0 / lhs_[k1][2];
            lhs_[k1][3] = fac1 * lhs_[k1][3];
            lhs_[k1][4] = fac1 * lhs_[k1][4];
            for (m = 0; m < 3; m++)
                rhs(k1, j, i, m) = fac1 * rhs(k1, j, i, m);

            lhs_[k2][2] = lhs_[k2][2] - lhs_[k2][1] * lhs_[k1][3];
            lhs_[k2][3] = lhs_[k2][3] - lhs_[k2][1] * lhs_[k1][4];
            for (m = 0; m < 3; m++)
                rhs(k2, j, i, m) = rhs(k2, j, i, m) - lhs_[k2][1] * rhs(k1, j, i, m);

            fac2 = 1.0 / lhs_[k2][2];
            for (m = 0; m < 3; m++)
                rhs(k2, j, i, m) = fac2 * rhs(k2, j, i, m);
        }

        m = 3;
        fac1 = 1.0 / lhsp_[k - 1][2];
        lhsp_[k - 1][3] = fac1 * lhsp_[k - 1][3];
        lhsp_[k - 1][4] = fac1 * lhsp_[k - 1][4];
        rhs(k - 1, j, i, m) = fac1 * rhs(k - 1, j, i, m);

        lhsp_[k1][2] = lhsp_[k1][2] - lhsp_[k1][1] * lhsp_[k - 1][3];
        lhsp_[k1][3] = lhsp_[k1][3] - lhsp_[k1][1] * lhsp_[k - 1][4];
        rhs(k1, j, i, m) = rhs(k1, j, i, m) - lhsp_[k1][1] * rhs(k - 1, j, i, m);

        lhsp_[k2][1] = lhsp_[k2][1] - lhsp_[k2][0] * lhsp_[k - 1][3];
        lhsp_[k2][2] = lhsp_[k2][2] - lhsp_[k2][0] * lhsp_[k - 1][4];
        rhs(k2, j, i, m) = rhs(k2, j, i, m) - lhsp_[k2][0] * rhs(k - 1, j, i, m);

        m = 4;
        fac1 = 1.0 / lhsm_[k - 1][2];
        lhsm_[k - 1][3] = fac1 * lhsm_[k - 1][3];
        lhsm_[k - 1][4] = fac1 * lhsm_[k - 1][4];
        rhs(k - 1, j, i, m) = fac1 * rhs(k - 1, j, i, m);

        lhsm_[k1][2] = lhsm_[k1][2] - lhsm_[k1][1] * lhsm_[k - 1][3];
        lhsm_[k1][3] = lhsm_[k1][3] - lhsm_[k1][1] * lhsm_[k - 1][4];
        rhs(k1, j, i, m) = rhs(k1, j, i, m) - lhsm_[k1][1] * rhs(k - 1, j, i, m);

        lhsm_[k2][1] = lhsm_[k2][1] - lhsm_[k2][0] * lhsm_[k - 1][3];
        lhsm_[k2][2] = lhsm_[k2][2] - lhsm_[k2][0] * lhsm_[k - 1][4];
        rhs(k2, j, i, m) = rhs(k2, j, i, m) - lhsm_[k2][0] * rhs(k - 1, j, i, m);

        if (k == nz2)
        {
            m = 3;
            fac1 = 1.0 / lhsp_[k1][2];
            lhsp_[k1][3] = fac1 * lhsp_[k1][3];
            lhsp_[k1][4] = fac1 * lhsp_[k1][4];
            rhs(k1, j, i, m) = fac1 * rhs(k1, j, i, m);

            lhsp_[k2][2] = lhsp_[k2][2] - lhsp_[k2][1] * lhsp_[k1][3];
            lhsp_[k2][3] = lhsp_[k2][3] - lhsp_[k2][1] * lhsp_[k1][4];
            rhs(k2, j, i, m) = rhs(k2, j, i, m) - lhsp_[k2][1] * rhs(k1, j, i, m);

            m = 4;
            fac1 = 1.0 / lhsm_[k1][2];
            lhsm_[k1][3] = fac1 * lhsm_[k1][3];
            lhsm_[k1][4] = fac1 * lhsm_[k1][4];
            rhs(k1, j, i, m) = fac1 * rhs(k1, j, i, m);

            lhsm_[k2][2] = lhsm_[k2][2] - lhsm_[k2][1] * lhsm_[k1][3];
            lhsm_[k2][3] = lhsm_[k2][3] - lhsm_[k2][1] * lhsm_[k1][4];
            rhs(k2, j, i, m) = rhs(k2, j, i, m) - lhsm_[k2][1] * rhs(k1, j, i, m);

            rhs(k2, j, i, 3) = rhs(k2, j, i, 3) / lhsp_[k2][2];
            rhs(k2, j, i, 4) = rhs(k2, j, i, 4) / lhsm_[k2][2];

            for (m = 0; m < 3; m++)
                rhs(k1, j, i, m) = rhs(k1, j, i, m) - lhs_[k1][3] * rhs(k2, j, i, m);

            rhs(k1, j, i, 3) = rhs(k1, j, i, 3) - lhsp_[k1][3] * rhs(k2, j, i, 3);
            rhs(k1, j, i, 4) = rhs(k1, j, i, 4) - lhsm_[k1][3] * rhs(k2, j, i, 4);
        }
    }

    for (k = nz2; k >= 1; k--)
    {
        k1 = k;
        k2 = k + 1;

        for (m = 0; m < 3; m++)
            rhs(k - 1, j, i, m) = rhs(k - 1, j, i, m) - lhs_[k - 1][3] * rhs(k1, j, i, m) - lhs_[k - 1][4] * rhs(k2, j, i, m);

        rhs(k - 1, j, i, 3) = rhs(k - 1, j, i, 3) - lhsp_[k - 1][3] * rhs(k1, j, i, 3) - lhsp_[k - 1][4] * rhs(k2, j, i, 3);
        rhs(k - 1, j, i, 4) = rhs(k - 1, j, i, 4) - lhsm_[k - 1][3] * rhs(k1, j, i, 4) - lhsm_[k - 1][4] * rhs(k2, j, i, 4);
    }
}

__global__ void z_post(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
                       double (*us)      [P_SIZE][P_SIZE],
                       double (*vs)      [P_SIZE][P_SIZE],
                       double (*ws)      [P_SIZE][P_SIZE],
                       double (*qs)      [P_SIZE][P_SIZE],
                       double (*rho_i)   [P_SIZE][P_SIZE],
                       double (*speed)   [P_SIZE][P_SIZE],
                       double (*square)  [P_SIZE][P_SIZE],
                       double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
                       double (*forcing) [P_SIZE][P_SIZE][P_SIZE],
                       params *pp,
                       unsigned nx2,
                       unsigned ny2,
                       unsigned nz2)
{
    unsigned i = blockIdx.x * blockDim.x + threadIdx.x + 1;
    unsigned j = blockIdx.y * blockDim.y + threadIdx.y + 1;
    unsigned k = blockIdx.z * blockDim.z + threadIdx.z + 1;
    if (k > nz2 || j > ny2 || i > nx2)
        return;

    double t1, t2, t3, ac, xvel, yvel, zvel;
    double btuz, ac2u, uzik1, r1, r2, r3, r4, r5;

    xvel = us[k][j][i];
    yvel = vs[k][j][i];
    zvel = ws[k][j][i];
    ac = speed[k][j][i];

    ac2u = ac*ac;

    r1 = rhs(k, j, i, 0);
    r2 = rhs(k, j, i, 1);
    r3 = rhs(k, j, i, 2);
    r4 = rhs(k, j, i, 3);
    r5 = rhs(k, j, i, 4);

    uzik1 = u(k, j, i, 0);
    btuz = pp->bt * uzik1;

    t1 = btuz / ac * (r4 + r5);
    t2 = r3 + t1;
    t3 = btuz * (r4 - r5);

    rhs(k, j, i, 0) = t2;
    rhs(k, j, i, 1) = -uzik1*r2 + xvel*t2;
    rhs(k, j, i, 2) = uzik1*r1 + yvel*t2;
    rhs(k, j, i, 3) = zvel*t2 + t3;
    rhs(k, j, i, 4) = uzik1*(-xvel*r2 + yvel*r1) + qs[k][j][i] * t2 + pp->c2iv*ac2u*t1 + zvel*t3;
}

//---------------------------------------------------------------------
// this function performs the solution of the approximate factorization
// step in the z-direction for all five matrix components
// simultaneously. The Thomas algorithm is employed to solve the
// systems for the z-lines. Boundary conditions are non-periodic
//---------------------------------------------------------------------
void z_solve(double (*u)       [P_SIZE][P_SIZE][P_SIZE],
             double (*us)      [P_SIZE][P_SIZE],
             double (*vs)      [P_SIZE][P_SIZE],
             double (*ws)      [P_SIZE][P_SIZE],
             double (*qs)      [P_SIZE][P_SIZE],
             double (*rho_i)   [P_SIZE][P_SIZE],
             double (*speed)   [P_SIZE][P_SIZE],
             double (*square)  [P_SIZE][P_SIZE],
             double (*rhs)     [P_SIZE][P_SIZE][P_SIZE],
             double (*forcing) [P_SIZE][P_SIZE][P_SIZE],
             params *pp,
             unsigned nx2,
             unsigned ny2,
             unsigned nz2)
{
    if (timeron) timer_start(t_zsolve);
    dim3 threads_per_block;
    dim3 blocks;
    compute_grid(threads_per_block, blocks, nx2, ny2);

    z_main<<<blocks, threads_per_block>>>(us, vs, ws, qs, rho_i, speed, square, rhs, pp, nx2, ny2, nz2);

    //---------------------------------------------------------------------
    // block-diagonal matrix-vector multiplication                       
    //---------------------------------------------------------------------
    if (timeron) timer_start(t_tzetar);
    compute_grid(threads_per_block, blocks, nx2, ny2, nz2);
    z_post<<<blocks, threads_per_block>>>(u, us, vs, ws, qs, rho_i, speed, square, rhs, forcing, pp, nx2, ny2, nz2);

    if (timeron) timer_stop(t_tzetar);
    if (timeron) timer_stop(t_zsolve);
}
